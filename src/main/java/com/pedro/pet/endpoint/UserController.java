package com.pedro.pet.endpoint;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pedro.pet.error.NameAlreadyExistsException;
import com.pedro.pet.error.ResourceNotFoundException;
import com.pedro.pet.model.User;
import com.pedro.pet.service.impl.UserServiceImpl;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserServiceImpl service;
	
	@PostMapping
	@Transactional
	public ResponseEntity<User> newUser(@RequestBody User user){
		if(service.verifyUserName(user) == true) {
			return new ResponseEntity<User>(service.newUser(user),HttpStatus.CREATED);
		}else {
			throw new NameAlreadyExistsException("Já existe um usuário de nome " + user.getName() + "!");
		}
		
	}
	
	@GetMapping
	public ResponseEntity<Iterable<User>> listUsers(){
		return new ResponseEntity<>(service.listUsers(), HttpStatus.OK);
	}
	
	@DeleteMapping("/{name}")
	@Transactional
	public ResponseEntity<String> deletarTudo(@PathVariable String name){
		name = name.replaceAll("-", " ");
		User user = verifyIfUserExists(name);
		Long id = user.getId();
		service.deleteUser(id);
		return new ResponseEntity<String>("O usuario de Id:"+ id + " e nome:" + name + " foi apagado!", HttpStatus.OK);
	}
	
	@GetMapping("/{name}")
	public ResponseEntity<User> findUsersByName(@PathVariable String name){
			User user = verifyIfUserExists(name);
			return new ResponseEntity<>(user, HttpStatus.OK);
		
	}
	
	@PutMapping
	@Transactional
	public ResponseEntity<User> updateUser(@RequestBody User user){
			 verifyIfUserExistsById(user.getId());
			 return new ResponseEntity<User>(service.newUser(user),HttpStatus.OK);
	}
	
	private User verifyIfUserExists(String name ) {
		User user = service.getUser(name);
		if(user == null) {
			throw new ResourceNotFoundException("Não há nenhum usuário Chamado " + name + " no Sistema");
		} else {
			return user;
		}
	}
	private boolean verifyIfUserExistsById(Long id ) {
		User nUser = service.getUserById(id);
		if(nUser == null) {
			throw new ResourceNotFoundException("Não há nenhum usuário com o Id:" + id + " no Sistema");
		} else {
			return true;
		}
		
	}

}
