package com.pedro.pet.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.pedro.pet.error.NameAlreadyExistsDetail;
import com.pedro.pet.error.NameAlreadyExistsException;
import com.pedro.pet.error.ResourceNotFoundDetails;
import com.pedro.pet.error.ResourceNotFoundException;

@ControllerAdvice
public class RestExceptionHandler {
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException rnfException){
		ResourceNotFoundDetails rnfDetails = ResourceNotFoundDetails.ResourceNotFoundDetailBuilder.newbuilder()
		.Timestamp(new Date().getTime())
		.Status(HttpStatus.NOT_FOUND.value())
		.Title("Resource Not Found Exception")
		.Detail(rnfException.getMessage())
		.DeveloperMessage(rnfException.getClass().getName())
		.build();
		
		return new ResponseEntity<>(rnfDetails, HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(NameAlreadyExistsException.class)
	public ResponseEntity<?> handleNameAlreadyExists(NameAlreadyExistsException naeException){
		 NameAlreadyExistsDetail naeDetails = NameAlreadyExistsDetail.newBuilder()
		.Timestamp(new Date().getTime())
		.Status(HttpStatus.BAD_REQUEST.value())
		.Title("Name Already Exists Exception")
		.Detail(naeException.getMessage())
		.DeveloperMessage(naeException.getClass().getName())
		.build();
		
		return new ResponseEntity<>(naeDetails, HttpStatus.BAD_REQUEST);
	}


}
