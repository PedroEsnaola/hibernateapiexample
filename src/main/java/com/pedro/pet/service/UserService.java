package com.pedro.pet.service;

import org.springframework.stereotype.Component;

import com.pedro.pet.model.User;

@Component
public interface UserService {
	
	public Iterable<User> listUsers();
	
	public User newUser(User user);
	
	public User updateUser(User user);
	
	public String deleteUser(Long id);
	
	public User getUser(String name);
	
	public Boolean verifyUserName(User user);
	
	public User getUserById(Long id);
	

}
