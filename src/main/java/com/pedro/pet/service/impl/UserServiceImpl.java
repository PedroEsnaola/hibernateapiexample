package com.pedro.pet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pedro.pet.model.User;
import com.pedro.pet.repository.UserRepository;
import com.pedro.pet.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository repository;

	@Override
	public Iterable<User> listUsers() {
		return repository.findAll();
		
	}

	@Override
	public User newUser(User user) {
		return repository.save(user);
	}

	@Override
	public User updateUser(User user) {
		Long id = repository.count();
		user.setId(id);
		return repository.save(user);
	}

	@Override
	public String deleteUser(Long id) {
		repository.deleteById(id);
		String resposta = "O usuário foi excluido com sucesso";
		return resposta;
	}

	@Override
	public User getUser(String name) {
		
		return repository.findByNameIgnoreCaseContaining(name);
	}
	@Override
	public Boolean verifyUserName(User user) {
		if(repository.findByName(user.getName()) == null) {
			return true;
		}else { 
			return false;
		}
		
	}

	@Override
	public User getUserById(Long id) {
		return repository.findById(id).orElse(null);
	}
	

}
