package com.pedro.pet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pedro.pet.model.Item;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
	
	public Item findByName(String name);

}
