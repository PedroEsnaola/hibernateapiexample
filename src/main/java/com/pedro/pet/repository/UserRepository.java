package com.pedro.pet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pedro.pet.model.User;


@Repository
public interface UserRepository extends CrudRepository<User, Long>{
	
	public User findByNameIgnoreCaseContaining(String name);
	public User findByName(String name);


}
