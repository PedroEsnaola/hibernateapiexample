package com.pedro.pet.error;

public class ErrorDetail {

	String title;
	int Status;
	String detail;
	Long timestamp;
	String DeveloperMessage;
	
	public String getTitle() {
		return title;
	}

	public int getStatus() {
		return Status;
	}

	public String getDetail() {
		return detail;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public String getDeveloperMessage() {
		return DeveloperMessage;
	}
}
