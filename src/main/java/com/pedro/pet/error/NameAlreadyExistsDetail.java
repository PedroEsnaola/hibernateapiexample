package com.pedro.pet.error;

import javax.annotation.Generated;

public class NameAlreadyExistsDetail extends ErrorDetail {

	@Generated("SparkTools")
	private NameAlreadyExistsDetail(Builder builder) {
		this.title = builder.title;
		this.Status = builder.Status;
		this.detail = builder.detail;
		this.timestamp = builder.timestamp;
		this.DeveloperMessage = builder.DeveloperMessage;
	}
	
		private NameAlreadyExistsDetail() {
		}

	/**
	 * Creates builder to build {@link NameAlreadyExistsDetail}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder newBuilder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link NameAlreadyExistsDetail}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private String title;
		private int Status;
		private String detail;
		private Long timestamp;
		private String DeveloperMessage;

		private Builder() {
		}

		public Builder Title(String title) {
			this.title = title;
			return this;
		}

		public Builder Status(int Status) {
			this.Status = Status;
			return this;
		}

		public Builder Detail(String detail) {
			this.detail = detail;
			return this;
		}

		public Builder Timestamp(Long timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public Builder DeveloperMessage(String DeveloperMessage) {
			this.DeveloperMessage = DeveloperMessage;
			return this;
		}

		public NameAlreadyExistsDetail build() {
			return new NameAlreadyExistsDetail(this);
		}
	}
	
 

}
