package com.pedro.pet.error;

import javax.annotation.Generated;

public class ResourceNotFoundDetails extends ErrorDetail {
	
	

	@Generated("SparkTools")
	private ResourceNotFoundDetails(ResourceNotFoundDetailBuilder builder) {
		this.title = builder.title;
		this.Status = builder.Status;
		this.detail = builder.detail;
		this.timestamp = builder.timestamp;
		this.DeveloperMessage = builder.DeveloperMessage;
	}
	
	private ResourceNotFoundDetails() {
		
	}

	

	/**
	 * Creates builder to build {@link ResourceNotFoundExceptionDetails}.
	 * @return created builder
	 */


	/**
	 * Builder to build {@link ResourceNotFoundExceptionDetails}.
	 */
	public static final class ResourceNotFoundDetailBuilder {
		private String title;
		private int Status;
		private String detail;
		private Long timestamp;
		private String DeveloperMessage;

		private ResourceNotFoundDetailBuilder() {
		}
		

		public static ResourceNotFoundDetailBuilder newbuilder() {
			return new ResourceNotFoundDetailBuilder();
		}

		public ResourceNotFoundDetailBuilder Title(String title) {
			this.title = title;
			return this;
		}

		public ResourceNotFoundDetailBuilder Status(int Status) {
			this.Status = Status;
			return this;
		}

		public ResourceNotFoundDetailBuilder Detail(String detail) {
			this.detail = detail;
			return this;
		}

		public ResourceNotFoundDetailBuilder Timestamp(Long timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public ResourceNotFoundDetailBuilder DeveloperMessage(String DeveloperMessage) {
			this.DeveloperMessage = DeveloperMessage;
			return this;
		}

		public ResourceNotFoundDetails build() {
			return new ResourceNotFoundDetails(this);
		}
	}

}
